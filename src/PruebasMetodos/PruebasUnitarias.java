package PruebasMetodos;

import MisApis.DiccionarioMultipleTDA;
import MisApis.PilaTDA;
import MisImplementacion.Dinamicas.DiccionarioMultiple;
import MisImplementacion.Estaticas.Pila;
import Utilidades.EntrSal;
import Utilidades.MetodosTDA;

public class PruebasUnitarias {
	public void pruebaPasarPilaPila() {
		PilaTDA o, d;
		o = new Pila();
		o.inicializarPila();
		d = new Pila();
		d.inicializarPila();
		
		EntrSal ei = new EntrSal();		
		ei.cargarPila(o);
		
		MetodosTDA m = new MetodosTDA();
		m.pasarPilaPila(d, o);
		
		System.out.println("Origen");
		ei.mostrarPila(o);
		
		System.out.println("Destino");
		ei.mostrarPila(d);
	}
	
	public void pruebaCopiarPilaPila() {
		PilaTDA o, d;
		o = new Pila();
		o.inicializarPila();
		d = new Pila();
		d.inicializarPila();
		
		EntrSal ei = new EntrSal();		
		ei.cargarPila(o);
		
		MetodosTDA m = new MetodosTDA();
		m.copiarPilaPila(d, o);
		
		System.out.println("Origen");
		ei.mostrarPila(o);
		
		System.out.println("Destino");
		ei.mostrarPila(d);
	}
	
	public void pruebaImplementacionDiccionarioMultiple() {
		DiccionarioMultipleTDA o;
		o = new DiccionarioMultiple();
		o.inicializarDiccionarioMultiple();

		
		EntrSal ei = new EntrSal();		
		ei.cargarDM(o);		

		ei.mostrarDM(o);
		
		o.eliminarValor(5, 6);
		o.eliminarValor(1,  923);
		ei.mostrarDM(o);
		 
	}

}
