package Utilidades;

import MisApis.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;
import java.util.Scanner;

public class EntrSal {
	public void cargarPila(PilaTDA origen) {
		for (int i= 1 ; i <= 5 ; i += 1)
			origen.apilar(i);
	}
	
	public void cargarDM(DiccionarioMultipleTDA origen) {
		 origen.agregar(7, 771);
		 origen.agregar(1,  923);
		 origen.agregar(5, 6);
		 origen.agregar(4, 3);
		 origen.agregar(5, 25);
	}

	public void mostrarPila(PilaTDA origen) {
		while(! origen.pilaVacia()) {
			System.out.println(origen.tope());
			origen.desapilar();
		}
	}
	
	public void mostrarCola(ColaTDA origen) {
		while(! origen.colaVacia()) {
			System.out.println(origen.primero());
			origen.desacolar();
		}
	}
	
	public void mostrarColaPrioridad(ColaPrioridadTDA origen) {
		while(! origen.colaVacia()) {
			System.out.println("Prioridad: " + origen.prioridad() + " - Valor: " + origen.primero());
			origen.desacolar();
		}
	}
	
	public void mostrarDM(DiccionarioMultipleTDA origen) {
		System.out.println("Listado valores diccionario multiple ");
		ConjuntoTDA cjtoClaves = origen.claves();
		while (!cjtoClaves.conjuntoVacio()) {
			int c = cjtoClaves.elegir();
			cjtoClaves.sacar(c);
			ConjuntoTDA cjtoValores = origen.recuperar(c);
			System.out.print("Clave: " + c + " - Valores: ");
			while(!cjtoValores.conjuntoVacio()) {
				int x = cjtoValores.elegir();
				cjtoValores.sacar(x);
				System.out.print(x  + "; ");
			}
			System.out.println();
		}
	}

	
	/**
	 * @Tarea: CargarPila: carga la pila destino con n�meros enteros , mientras no se
	 * ingrese 0
	 */
	public  void CargarPilaTeclado(PilaTDA destino) {
		Scanner teclado = new Scanner(System.in); // para uso de teclado
		int dato;

		System.out.print("Dato? ");
		dato = teclado.nextInt();
		while (dato != 0) {
			destino.apilar(dato);
			System.out.print("Dato? ");
			dato = teclado.nextInt();
		}
	}
	
	/**
	 * @Tarea: GenerarPila: genera la pila destino con 10  n�meros al azar entre 1 y 6
	 */
	public  void GenerarPila(PilaTDA destino) {
		final int   _CANT = 10;
		final int   _SUP =   6;
		final int   _INF =   1;

		Random randomGenerator = new Random();
		int dato;
		for (int i = 1; i <= _CANT; i++) {
			dato = randomGenerator.nextInt((_SUP - _INF + 1)) + _INF;
			destino.apilar(dato);
		}
	}	

	/**
	 * @Tarea: CargarPilaArch: carga la pila destino con n�meros enteros , mientras no sea
	 * fin de archivo
	 */
	public  void CargarPilaArch(PilaTDA destino){
		try {
	      FileReader arch = new FileReader("pila.csv");
	      BufferedReader buffer = new BufferedReader(arch);
	      int cual;
	      
	      String linea;
	      while((linea = buffer.readLine()) != null) {
 	  
	    	  cual = Integer.valueOf(linea);
	    	  destino.apilar(cual);
	      }
	      arch.close();
	    }
	    catch(Exception e) {
	      System.out.println("Excepcion leyendo archivo "+ "pila.csv" + ": " + e);
	    }		
	}


}
