package Utilidades;

import MisApis.ColaPrioridadTDA;
import MisApis.ConjuntoTDA;
import MisApis.PilaTDA;
import MisImplementacion.Estaticas.ColaPrioridad;
import MisImplementacion.Estaticas.Pila;

public class MetodosTDA {
	
	public void pasarPilaPila(PilaTDA dest, PilaTDA origen) {
		while (!origen.pilaVacia()) {
			dest.apilar(origen.tope());
			origen.desapilar();
		}
	}
	
	public void copiarPilaPila(PilaTDA dest, PilaTDA origen) {
		PilaTDA aux;
		aux = new Pila();
		aux.inicializarPila();
		pasarPilaPila(aux, origen);
		while (!aux.pilaVacia()) {
			origen.apilar(aux.tope());
			dest.apilar(aux.tope());
			aux.desapilar();
		}
	}
 /***-----------------------------------------------------------------***/	
	/*** Precond: los dos conj. deben estar inicializados ***/
	/*** Postcondición: los conjuntos c1 y c2 quedan en estado indefinido ***/
	public boolean conjuntosIguales (ConjuntoTDA c1, ConjuntoTDA c2) {
		int e, cant = 0;
		while (!c1.conjuntoVacio() && cant == 0) {
			e = c1.elegir();
			if (c2.pertenece(e))
				c2.sacar(e);
			else {
				cant = 1;	
			}
			c1.sacar(e);
		}
		if (cant == 0 && c2.conjuntoVacio())
			return true; // c1 = {1, 2, 4} - c2 = {2, 1, 4}
		else 
			return false; // c1 = {1, 2, 4} - c2 {5, 4, 2, 1} --> cant = 0 * c1 = {1, 2, 4} - c2 {5, 4, 1}  --> cant = 1
	}

/***-----------------------------------------------------------------***/	
	public void vaciarConjunto(ConjuntoTDA o) {
		int e;
		while (!o.conjuntoVacio()) {
			e = o.elegir();
			o.sacar(e);
		}
	}
	
	public void copiarCPCP(ColaPrioridadTDA d, ColaPrioridadTDA o) {
		ColaPrioridadTDA aux = new ColaPrioridad();
		aux.inicializarCola();
		
		while(!o.colaVacia()) {
			aux.acolarPrioridad(o.primero(), o.prioridad());
			o.desacolar();
		}
		
		while(!aux.colaVacia()) {
			o.acolarPrioridad(aux.primero(),aux.prioridad());
			d.acolarPrioridad(aux.primero(), aux.prioridad());
			aux.desacolar();
		}
		
	}
	
	public void vaciarColaPrioridad(ColaPrioridad o) {
		while (!o.colaVacia()) {
			o.desacolar();
		}
	}


}
