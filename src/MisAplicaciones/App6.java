package MisAplicaciones;

import MisApis.ConjuntoTDA;
import MisImplementacion.Estaticas.Conjunto;
import Utilidades.MetodosTDA;

public class App6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConjuntoTDA c1, c2;
		c1 = new Conjunto();
		c1.inicializarConjunto();
		c2 = new Conjunto();
		c2.inicializarConjunto();
		
		for (int i = 1; i <= 5; i += 1) 
			c1.agregar(i);		
		c1.agregar(11);
		c1.agregar(12);
		
		for (int i = 3; i <= 8; i += 1)
			c2.agregar(i);

		// interseccion c1 y c2 = {3, 4, 5}
		MetodosTDA m = new MetodosTDA();
		System.out.println("interseccion c1 y c2 = {3, 4, 5}");
		if (m.conjuntosIguales(c1, c2))
			System.out.println("Conj. iguales");
		else 
			System.out.println("Conj. no iguales");
		 
		m.vaciarConjunto(c2);			
		m.vaciarConjunto(c1);	
		
		for (int i = 1; i <= 5; i += 1)
			c1.agregar(i);
		
		
		for (int i = 5; i >= 1; i -= 1)
			c2.agregar(i);

		// interseccion c1 y c2 = {1, 2, 3, 4, 5}
		
		System.out.println("interseccion c1 y c2 = {1, 2, 3, 4, 5}");
		if (m.conjuntosIguales(c1, c2))
			System.out.println("Conj. iguales");
		else 
			System.out.println("Conj. no iguales");
	}

}
