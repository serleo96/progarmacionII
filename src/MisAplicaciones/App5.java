package MisAplicaciones;

import MisApis.ColaPrioridadTDA;
import MisImplementacion.Estaticas.ColaPrioridad;
import Utilidades.EntrSal;
import Utilidades.MetodosTDA;

public class App5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntrSal ei = new EntrSal(); 		// acceso m�todos para ingresar datos y mostrar
		MetodosTDA m = new MetodosTDA();    // acceso a m�odos que usan TDA's
		
		ColaPrioridadTDA o;
		o = new ColaPrioridad();
		o.inicializarCola();
		
		ColaPrioridadTDA d;
		d = new ColaPrioridad();
		d.inicializarCola();
		
		o.acolarPrioridad(81, 7);
		o.acolarPrioridad(99, 1);
		o.acolarPrioridad(76, 5);
		o.acolarPrioridad(28, 5);
		o.acolarPrioridad( 9, 2);
			

	//	d = o; NO copia , son d y o la misma cola
	//	ei.mostrarColaPrioridad(d);
		
		m.copiarCPCP(d, o);
		
		System.out.println("Origen");
		ei.mostrarColaPrioridad(o);
		d.acolarPrioridad(1, 11);
		System.out.println("Destino");
		ei.mostrarColaPrioridad(d);
	}

}
