package MisAplicaciones;

import MisApis.PilaTDA;
import MisImplementacion.Estaticas.Pila;

public class App2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PilaTDA origen; 
		origen = new Pila();
		
		origen.inicializarPila();
		
		for (int i = 1; i <= 4; i +=1)
			origen.apilar(i);
		
		while (! origen.pilaVacia()) {
			System.out.println(origen.tope());
			origen.desapilar();
		}

	}

}
