package MisAplicaciones;

import MisApis.PilaTDA;
import MisImplementacion.Estaticas.Pila;
import Utilidades.EntrSal;
import Utilidades.MetodosTDA;

public class App3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PilaTDA o, d;
		o = new Pila();
		o.inicializarPila();
		d = new Pila();
		d.inicializarPila();
		
		EntrSal ei = new EntrSal();		
		ei.cargarPila(o);
		
		MetodosTDA m = new MetodosTDA();
		m.pasarPilaPila(d, o);
		
		System.out.println("Origen");
		ei.mostrarPila(o);
		
		System.out.println("Destino");
		ei.mostrarPila(d);

	}

}
