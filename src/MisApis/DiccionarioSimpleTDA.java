package MisApis;

public interface DiccionarioSimpleTDA {
	void inicializarDiccionarioSimple();

    /**
     * agrega un elemneto al diccionario Simple,
	 * para esto el diccionario Simple debe estar inicializado
	 * @param c
     * @param x
	 */
	void agregar(int c, int x);


    /**
     * elimina un elmento del diccionario simple,
	 * para esto el diccionario Simple debe estar inicializado
	 * @param c
	 */
	void eliminar(int c);

    /**
     * recuper un elemento del diccionaro simple,
	 * para esto el diccionario Simple debe star inicializado
	 * @param c
     * @return
     */
	int recuperar(int c);

    /**
     * devuelve las claves de un diccionaro simple,
	 * para esto el diccionario Simple debe star inicializado
	 * @return
     */
    ConjuntoTDA claves();
}
