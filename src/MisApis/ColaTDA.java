package MisApis;

public interface ColaTDA {
	/**
	 * inicializa cola
	 */
	void inicializarCola();


	/**
	 * acola un elemento a la cola,
	 * para esto la cola debe star inicializada
	 * @param x
	 */
	void acolar(int x);


	/**
	 * saca un elemneto de la cola,
	 * para esto la cola debe star inicializada
	 */
	void desacolar();


	/**
	 * retorna el primero,
	 * para esto la cola debe star inicializada
	 * @return
	 */
	int primero();

	/**
	 * retorna si la cola esta vacia
	 * @return
	 */
	boolean colaVacia();
}
