package MisApis;

public interface DiccionarioMultipleTDA {
	void inicializarDiccionarioMultiple();

    /**
     * agrega un elemento al diccionario multiple,
	 * para esto el diccionario multiple debe star inicializado
	 * @param c
     * @param x
	 */
	void agregar(int c, int x);

    /**
	 * quita un elemento del diccionario multiple
	 * para esto el diccionario multiple debe star inicializado
	 * @param c
	 */
	void eliminar(int c);

    /**
     * recupera un elemento del diccionario multiple
	 * para esto el diccionario multiple debe star inicializado
	 * @param c
     * @return
     */
	ConjuntoTDA recuperar(int c);


    /**
     * elemina un valor de un elemento del un diccionario multiple,
	 * para esto el diccionario multiple debe star inicializado
	 * @param c
     * @param x
	 */
	void eliminarValor(int c, int x);


    /**
     * para esto el diccionario multiple debe star inicializado
	 * @return
     */
    ConjuntoTDA claves();
}
