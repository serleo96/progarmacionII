package MisApis;

public interface PilaTDA {
	void inicializarPila();

    /**
     * apila un elemento a la pila,
	 * para esto la pila debe estar inicilaizada
	 * @param x
	 */
	void apilar(int x);


    /**
     * desapila el ultimao elemento de la pila,
	 * para esto la pila debe estar inicilaizada
	 */
	void desapilar();


    /**
     * para esto la pila debe estar inicilaizada
	 * @return
     */
	int tope();


    /**
     * para esto la pila debe estar inicilaizada
	 * @return
     */
    boolean pilaVacia();
}
