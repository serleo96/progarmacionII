package MisApis;

public interface ColaPrioridadTDA {
	/**
     * inicializa la cola con prioridad
	 */
	void inicializarCola();


    /**
     * acola un elemmento al la cola
	 * para esto la cola debe star inicializada
	 * @param x valor
	 * @param p prioridad del elemento
	 */
	void acolarPrioridad(int x, int p);


    /**
	 * descaola un elemento del la cola
	 * para esto la cola debe star inicializada
	 */
	void desacolar();

	/**
	 * retorna el primero,
	 * para esto la cola debe star inicializada
	 * @return
	 */
	int primero();


    /**
     * retrona cola si la cola esta o no vacia
	 * @return
     */
	boolean colaVacia();

    /**
     * la cola debe estar inicializada
	 * @return
     */
    int prioridad();
}
