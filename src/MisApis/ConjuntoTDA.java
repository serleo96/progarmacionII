package MisApis;

public interface ConjuntoTDA {

    /**
     * inicializa el conjunto
	 */
	void inicializarConjunto();


    /**
	 * agregar el elemnto al conjunto,
	 * para esto el conjunto debe star inicializada
	 * @param x
	 */
	void agregar(int x);


    /**
     * saca un elemento del conjunto,
	 * para esto el conjunto debe star inicializada
	 * @param x
	 */
	void sacar(int x);


    /**
     * para esto el conjunto debe star inicializada
	 * @return
     */
	int elegir();


    /**
     * retorna si el conjunto esta o no vacio
	 * @return
     */
	boolean conjuntoVacio();

	/**
     * retorna si el elemento existe en el conjunto,
	 * para esto el conjunto debe star inicializada
	 * @param x
     * @return
     */
	boolean pertenece(int x);
}
