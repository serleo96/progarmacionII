package MisImplementacion.Dinamicas;

import MisApis.ConjuntoTDA;
import MisApis.DiccionarioMultipleTDA;
import MisImplementacion.Estaticas.Conjunto;

public class DiccionarioMultiple implements DiccionarioMultipleTDA {
	
	class Nodo {
		int clave;
		NodoValor inicioVal;
		Nodo sigClave;
	}
	
	class NodoValor{
		int valor;
		NodoValor sigValor;
	}
	
	Nodo inicioClave;

	public void inicializarDiccionarioMultiple() {
		inicioClave = null;
	}
	
	public ConjuntoTDA claves() {
		ConjuntoTDA resultado = new Conjunto();
		resultado.inicializarConjunto();		
		Nodo actual = inicioClave;
		while (actual != null) {
			resultado.agregar(actual.clave);
			actual = actual.sigClave;
		}				
		return resultado;
	}
	
	private Nodo buscarClave(int c) {
		Nodo actual = inicioClave;
		while (actual != null && actual.clave != c)
			actual = actual.sigClave;
		return actual;		
	}

	public ConjuntoTDA recuperar(int c) {	
		ConjuntoTDA resultado = new Conjunto();
		resultado.inicializarConjunto();		
		Nodo actualClave = buscarClave(c);
		if (actualClave != null) {
			NodoValor actual = actualClave.inicioVal;
			while (actual != null) {
				resultado.agregar(actual.valor);
				actual = actual.sigValor;
			}				
		}		
		return resultado;
	}	
	
	private NodoValor buscarValor(NodoValor inicio, int x) {
		NodoValor actual = inicio;
		while (actual != null && actual.valor != x)
			actual = actual.sigValor;
		return actual;		
	}
	
	public void agregar(int c, int x) {
		Nodo actualClave = buscarClave(c);
		if (actualClave == null) {
			Nodo nuevoClave = new Nodo();
			nuevoClave.clave = c;
			nuevoClave.inicioVal = null;
			nuevoClave.sigClave = inicioClave;
			inicioClave = nuevoClave;
			actualClave = nuevoClave;
		};
		NodoValor actualValor = buscarValor(actualClave.inicioVal, x);
		if (actualValor == null) {
			NodoValor nuevoValor = new NodoValor();
			nuevoValor.valor = x;
			nuevoValor.sigValor = actualClave.inicioVal;
			actualClave.inicioVal = nuevoValor;			
		}
	}	

	public void eliminar(int c) {
		Nodo actual = inicioClave, ant = null;
		while (actual != null && actual.clave != c) {
			ant = actual;
			actual = actual.sigClave;
		}
		if (actual != null)
			if (ant == null)
				inicioClave = inicioClave.sigClave;
			else
				ant.sigClave = actual.sigClave;
	}

	public void eliminarValor(int c, int x) {
		Nodo actualClave = buscarClave(c);
		if (actualClave != null){
			NodoValor actual = actualClave.inicioVal, ant = null;
			while (actual != null && actual.valor != x) {
				ant = actual;
				actual = actual.sigValor;
			}
			if (actual != null) {
				if (ant == null)
					actualClave.inicioVal = actualClave.inicioVal.sigValor;
				else
					ant.sigValor = actual.sigValor;
				if (actualClave.inicioVal == null)
					eliminar(actualClave.clave);
			}								
		}
	}

}
