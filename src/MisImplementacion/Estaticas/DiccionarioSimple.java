package MisImplementacion.Estaticas;

import MisApis.ConjuntoTDA;
import MisApis.DiccionarioSimpleTDA;

public class DiccionarioSimple implements DiccionarioSimpleTDA {

    class Elem {
        int cl, dato;
    }


    Elem[] vector;
    int cant;

    @Override
    public void inicializarDiccionarioSimple() {
        cant = 0;
        vector = new Elem[100];
    }

    @Override
    public void agregar(int c, int x) {
        int pos = cant - 1;

        while (pos >= 0 && vector[pos].cl != c)
            pos--;

        if (pos == -1) {
            pos = cant;
            vector[pos] = new Elem();
            vector[pos].cl = c;
            cant++;
        }
        vector[pos].dato = x;
    }

    @Override
    public void eliminar(int c) {
        int pos = cant - 1;

        while (pos >= 0 && vector[pos].cl != c)
            pos--;

        if (pos != -1) {
            vector[pos] = vector[cant - 1];
            cant--;
        }
    }

    @Override
    public int recuperar(int c) {
        int pos = cant - 1;

        while (pos >= 0 && vector[pos].cl != c)
            pos--;
        return vector[pos].dato;

    }

    @Override
    public ConjuntoTDA claves() {
		ConjuntoTDA c= new Conjunto() ;
		c.inicializarConjunto(); ;
		for ( int i =0; i < cant ; i ++) {
			c.agregar( vector[i].cl);
		}
		return c;
	}

}
