package MisImplementacion.Estaticas;

import MisApis.ColaTDA;

public class Cola implements ColaTDA {
	
	int [] vector;
	int poner, sacar;

	public void inicializarCola() {
		vector = new int [100];
		poner = sacar = 0;
	}

	public void acolar(int x) {
		vector[poner] = x;
		poner += 1;
		if (poner == 100)
			poner = 0;
	}

	@Override
	public void desacolar() {
		poner = sacar --;
	}

	@Override
	public int primero() {
		return vector[vector.length -1];
	}

	@Override
	public boolean colaVacia() {
		return ( vector.length == 0);
	}
	
	public static void pasarDeUnaColaAOtra(Cola a, Cola b) { //Ejercicio E
		
		Pila c = new Pila();
	
		while(!a.colaVacia()) {
			c.apilar(a.primero());
			a.desacolar();
		}
		
		while(!c.pilaVacia()) {
			b.acolar(c.tope());
			c.desapilar();
		}
		
	}
	
	public static void copiarUnaColaAOtra(Cola a, Cola b) { //Ejercicio F
		
		Cola c = new Cola();
		
		while(!a.colaVacia()) {
			c.acolar(a.primero());
			a.desacolar();
		}
		
		while(!c.colaVacia()) {
			a.acolar(c.primero());
			b.acolar(c.primero());
			c.desacolar();
		}
		
	}
	
	public static Cola invertirElContenido(Cola a) { //Ejercicio G
		
		Pila b = new Pila();
		
		while(!a.colaVacia()) {
			b.apilar(a.primero());
			a.desacolar();
		}
		
		while(!b.pilaVacia()) {
			a.acolar(b.tope());
			b.desapilar();
		}
		
		return a;
		
	}
	
	public static boolean iguales(Cola a, Cola b) { //Ejercicio H
		
		while(!a.colaVacia()){
			if(a.primero() == b.primero()) {
				a.desacolar();
				b.desacolar();
			} else {
				return false;
			}
		}
		
		return b.colaVacia();
		
	}
	
	public static void repartir(Cola c) { //Ejercicio Q
		
		Cola copia = new Cola();
		Cola aux1 = new Cola();
		
		Cola m1 = new Cola();
		Cola m2 = new Cola();
		
		int i = 0;
		
		while(!c.colaVacia()) {
			
			if(i % 2 == 0) {
				aux1.acolar(c.primero());
			}
			
			copia.acolar(c.primero());
			c.desacolar();
			
			i++;
			
		}
		
		while(!aux1.colaVacia()) {
			m1.acolar(copia.primero());
			copia.desacolar();
			aux1.desacolar();
		}
		
		while(!copia.colaVacia()) {
			m2.acolar(copia.primero());
			copia.desacolar();
		}
		
	}
	
	public static int secuenciaMasLarga(Cola c) { //Ejercicio R
		
		int max = 0;
		int aux = 0;
		
		while(!c.colaVacia()) {
			if(c.primero() == 0 ) {
				if(aux > max) {
					max = aux;
				}
				aux = 0;
			} else {
				aux++;
			}
			c.desacolar();
		}
		
		return max;
		
	}
	
}
