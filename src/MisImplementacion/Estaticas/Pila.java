package MisImplementacion.Estaticas;

import MisApis.PilaTDA;

public class Pila implements PilaTDA {

	int [] vector;
	int cant;
	
	@Override
	public void inicializarPila() {
		vector = new int[100];
		cant = 0;
	}

	@Override
	public void apilar(int x) {
		vector[cant] = x;
		cant += 1;
	}

	@Override
	public void desapilar() {
		cant -= 1;
	}

	@Override
	public int tope() {
		return vector[cant-1];
	}

	@Override
	public boolean pilaVacia() {
		return cant == 0;
	}
	
	public static void pasarDeUnaPilaAOtra(Pila a, Pila b) { // Ejercicio A
		
		while(!a.pilaVacia()) {
			b.apilar(a.tope());
			a.desapilar();
		}
		
		//Esto no copia, solo pasa
		
	}
	
	public static void copiarDeUnaPilaAOtra(Pila a, Pila b) { // Ejercicio B
		
		Pila c = new Pila();
		
		while(!a.pilaVacia()) {
			c.apilar(a.tope());
			a.desapilar();
		}
		
		while(!c.pilaVacia()) {
			a.apilar(c.tope());
			b.apilar(c.tope());
			c.desapilar();
		}
		
	}
	
	public static Pila invertirContenido(Pila a) { // Ejercicio C
		
		Pila b = new Pila();
		Pila c = new Pila();
		
		while(!a.pilaVacia()) {
			b.apilar(a.tope());
			a.desapilar();
		}
		
		while(!b.pilaVacia()) {
			c.apilar(b.tope());
			b.desapilar();
		}
		
		while(!c.pilaVacia()) {
			a.apilar(c.tope());
			c.desapilar();
		}
		
		return a;
		
	}
	
	public static boolean iguales(Pila a, Pila b) { // Ejercicio D
		
		while(!a.pilaVacia()) {
			if(a.tope() != b.tope()) {
				a.desapilar();
				b.desapilar();
			} else {
				return false;
			}
		}
		
		return b.pilaVacia();
		
	}
	
	public static boolean parteDe(Pila a, Pila b) { //Ejercicio O
		
		Pila copia = new Pila();
		Pila aux = new Pila();
		
		while(!a.pilaVacia()) {
			aux.apilar(a.tope());
			a.desapilar();
		}
		
		while(!aux.pilaVacia()) {
			a.apilar(aux.tope());
			copia.apilar(aux.tope());
			aux.desapilar();
		}
		
		//Hasta aca copie la pila
		
		while(!b.pilaVacia() && !a.pilaVacia()) {
			if(a.tope() != b.tope()) {
				b.desapilar();
			} else {
				break;
			}
		}
		
		while(!a.pilaVacia()) {
			if(a.tope() != b.tope()) {
				a.desapilar();
				b.desapilar();
			} else {
				return parteDe(copia, b);
			}
		}
		
		return true;
		
	}
	
	public static void eliminarRepetidos(Pila p) { //Ejercicio P
		
		Pila copia = new Pila();
		Pila aux = new Pila();
		
		while(!p.pilaVacia()) {
			aux.apilar(p.tope());
			p.desapilar();
		}
		
		while(!aux.pilaVacia()) {
			p.apilar(aux.tope());
			copia.apilar(aux.tope());
			aux.desapilar();
		}
		
		//Hasta aca copie la pila
		
		Conjunto c = new Conjunto();
		
		while(p.pilaVacia()) {
			c.agregar(p.tope());
			p.desapilar();
		}
		
		while(!copia.pilaVacia()) {
			if(c.pertenece(copia.tope())) {
				p.apilar(copia.tope());
			}
			copia.desapilar();
		}
		
	}
	
	
}
