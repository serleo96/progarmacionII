package MisImplementacion.Estaticas;

import MisApis.ColaPrioridadTDA;

public class ColaPrioridad implements ColaPrioridadTDA {
	
	class Elem{
		int valor, p;
	}
	
	Elem [] vector;
	int cant;

	public void inicializarCola() {
		vector = new Elem[100];
		cant = 0;
	}

	public void acolarPrioridad(int x, int p) {
		int i = cant-1;
		while (i >= 0 && p <= vector[i].p)  {
			vector[i+1] = vector[i];
			i -= 1;			
		}
		vector[i+1] = new Elem();	
		vector[i+1].p = p;
		vector[i+1].valor = x;
		cant +=1;	
	}

	@Override
	public void desacolar() {
		vector[cant-1] = null;
		cant --;
	}

	@Override
	public int primero() {
		return vector[cant-1].valor;
	}

	@Override
	public boolean colaVacia() {
		return (cant==0);
	}

	@Override
	public int prioridad() {
		return vector[cant-1].p;
	}

	public static void pasarColaPrioridad(ColaPrioridad a, ColaPrioridad b) { //Ejercicio I
		
		while(!a.colaVacia()) {
			b.acolarPrioridad(a.primero(), a.prioridad());
			a.desacolar();
		}
		
	}
	
	public static void copiarColaPriodad(ColaPrioridad a, ColaPrioridad b) { //Ejercicio J
		
		ColaPrioridad c = new ColaPrioridad();
		
		while(!a.colaVacia()) {
			c.acolarPrioridad(a.primero(), a.prioridad());
			a.desacolar();
		}
		while(!c.colaVacia()) {
			a.acolarPrioridad(c.primero(), c.prioridad());
			b.acolarPrioridad(c.primero(), c.prioridad());
			c.desacolar();
		}
		
	}
	
	public static boolean iguales(ColaPrioridad a, ColaPrioridad b) { //Ejercicio K
	
		while(!a.colaVacia()) {
			if(a.primero() == b.primero() && a.prioridad() == b.prioridad()) {
				a.desacolar();
				b.desacolar();
			} else {
				return false;
			}
		}
		
		return b.colaVacia();
		
	}	
}


