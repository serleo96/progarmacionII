package MisImplementacion.Estaticas;

import MisApis.ConjuntoTDA;

public class Conjunto implements ConjuntoTDA {

    int[] vector;
    int cant;

    @Override
    public void inicializarConjunto() {
        vector = new int[100];
        cant = 0;
    }

    @Override
    public void agregar(int x) {
        if (!pertenece(x)) {
            vector[cant] = x;
            cant++;
        }
    }

    @Override
    public void sacar(int x) {
        int i = 0;
        while (i < cant && vector[i] != x)
            i++;
        if (i < cant) {
            vector[i] = vector[cant - 1];
            cant --;
        }
    }

    @Override
    public int elegir() {
        return vector[cant - 1];
    }

    @Override
    public boolean conjuntoVacio() {
        return cant == 0;
    }

    @Override
    public boolean pertenece(int x) {
        int i = 0;
        while (i < cant && vector[i] != x)
            i++;
        return (i < cant);
    }
	
	public static void pasarDeUnConjuntoAOtro(Conjunto a, Conjunto b) { //Ejercicio L
		
		while(!a.conjuntoVacio()) {
			int aux = a.elegir();
			b.agregar(aux);
			a.sacar(aux);
		}
		
	}
	
	public static void copiarDeUnConjuntoAOtro(Conjunto a, Conjunto b) { //Ejercicio M
		
		Conjunto c = new Conjunto();
		
		while(!a.conjuntoVacio()) {
			int aux = a.elegir();
			c.agregar(aux);
			a.sacar(aux);
		}
		
		while(!c.conjuntoVacio()) {
			int aux = c.elegir();
			a.agregar(aux);
			b.agregar(aux);
			c.sacar(aux);
		}
		
	}
	
	public static boolean iguales(Conjunto a, Conjunto b) { //Ejercicio N
		
		while(!a.conjuntoVacio()) {
			int aux = a.elegir();
			if(!b.pertenece(aux)) {
				return false;
			} else {
				a.sacar(aux);
				b.sacar(aux);
			}
		}
		
		return b.conjuntoVacio();
		
	}
	
	/*Ejercicio S*/

	public static Conjunto interseccion(Conjunto a, Conjunto b) {
		
		Conjunto c = new Conjunto();
		Conjunto d = new Conjunto();
		Conjunto.copiarDeUnConjuntoAOtro(a, d);
		
		while(!a.conjuntoVacio()) {
			int aux = a.elegir();
			if(a.pertenece(aux) && b.pertenece(aux)) {
				c.agregar(aux);
				a.sacar(aux);
				b.sacar(aux);
			}
		}
		
		a = d;
		
		return c;
		
	}
	
	public static Conjunto union(Conjunto a, Conjunto b) {
		
		Conjunto c = new Conjunto();
		Conjunto d = new Conjunto();
		Conjunto.copiarDeUnConjuntoAOtro(a, d);
		
		while(!a.conjuntoVacio()) {
			int aux = a.elegir();
			c.agregar(aux);
			a.sacar(aux);
		}
		
		a = d;
		Conjunto.copiarDeUnConjuntoAOtro(b, d);
		
		while(!b.conjuntoVacio()) {
			int aux = b.elegir();
			c.agregar(aux);
			b.sacar(aux);
		}
		
		b = d;
		
		return c;
		
	}
	
	public static Conjunto diferencia(Conjunto a, Conjunto b) { // A-B
		
		Conjunto d = new Conjunto();
		Conjunto.copiarDeUnConjuntoAOtro(b, d);
		
		while(!b.conjuntoVacio()) {
			int aux = b.elegir();
			a.sacar(aux);
			b.sacar(aux);
		}
		
		b = d;
		
		return a;
		
	}
	
	public static Conjunto diferenciaSimetrica(Conjunto a, Conjunto b) { //
		
		return Conjunto.diferencia(Conjunto.union(a, b), Conjunto.interseccion(a, b));
		
	}
	
>>>>>>> Stashed changes
}
